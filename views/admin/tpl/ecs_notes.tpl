[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]
[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box=" "}]
[{if !$ecs_notes_tab}][{assign var="ecs_notes_tab" value='1'}][{/if}]
[{* ################################################################################################################################################################## *}]
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Das neueste kompilierte und minimierte JavaScript -->
<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style type="text/css">ul li{list-style:inside;list-style:none;padding:0;margin:0;background:none}
.container{width:97%}
.nav-tabs li input{border:none;margin:2px;cursor:pointer;}
</style>
[{* ################################################################################################################################################################## *}]
[{if $ecs_notes_tiny}]
    <script src="[{$oViewConf->getModuleUrl('ecs/Notizen','Core/tinymce/tinymce.min.js')}]"></script>
    <script>
    tinymce.init({
        selector: 'textarea',
        plugins: [
                "advlist autolink autosave link image lists print hr anchor pagebreak ",
                "searchreplace wordcount code fullscreen  media",
                "table contextmenu directionality textcolor paste colorpicker textpattern"
        ],
        toolbar: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | cut copy paste | searchreplace | bullist numlist | undo redo | link unlink image | blockquote code |  forecolor backcolor | table | hr removeformat | print fullscreen ",
        menubar: false,
        toolbar_items_size: 'small',
        height : 450
    });
    </script>
[{/if}]
[{* ################################################################################################################################################################## *}]
<body>
    <div class="container">
        [{ if $readonly }] [{assign var="readonly" value="readonly disabled"}] [{else}] [{assign var="readonly" value=""}] [{/if}]
        <form name="transfer" id="transfer" action="[{ $oViewConf->getSelfLink() }]" method="post">     [{ $oViewConf->getHiddenSid() }]
            <input type="hidden" name="oxid" value="[{ $oxid }]">
            <input type="hidden" name="cl" value="ecs_notes">
            <input type="hidden" name="fnc" value="">
            <input type="hidden" name="actshop" value="[{$oViewConf->getActiveShopId()}]">
            <input type="hidden" name="updatenav" value="">
            <input type="hidden" name="editlanguage" value="[{ $editlanguage }]">
        </form>
        <form name="myedit" id="myedit" action="[{ $oViewConf->getSelfLink() }]" method="post">     [{ $oViewConf->getHiddenSid() }]
            <input type="hidden" name="cl" value="ecs_notes">
            <input type="hidden" name="fnc" value="">
            <input type="hidden" name="language" value="[{ $actlang }]">
            <h1>[{ oxmultilang ident="ECS_NOTES" }] </h1>
            [{* ################################################################################################################################################################## *}]
            <ul class="nav nav-tabs">
                <li[{if $ecs_notes_tab == "1"}] class="active"[{/if}] onclick="javascript:document.getElementById('activetab').value='1'"><a href="#notiz1" data-toggle="tab"><input type="text" class="editinput" size="10" name="editval[ecs_notes_ti]" placeholder="Notiz #1" value="[{$ecs_notes_ti}]"></a></li>
                <li[{if $ecs_notes_tab == "2"}] class="active"[{/if}] onclick="javascript:document.getElementById('activetab').value='2'"><a href="#notiz2" data-toggle="tab"><input type="text" class="editinput" size="10" name="editval[ecs_notes2_ti]" placeholder="Notiz #2" value="[{$ecs_notes2_ti}]"></a></li>
                <li[{if $ecs_notes_tab == "3"}] class="active"[{/if}] onclick="javascript:document.getElementById('activetab').value='3'"><a href="#notiz3" data-toggle="tab"><input type="text" class="editinput" size="10" name="editval[ecs_notes3_ti]" placeholder="Notiz #3" value="[{$ecs_notes3_ti}]"></a></li>
                <li[{if $ecs_notes_tab == "4"}] class="active"[{/if}] onclick="javascript:document.getElementById('activetab').value='4'"><a href="#notiz4" data-toggle="tab"><input type="text" class="editinput" size="10" name="editval[ecs_notes4_ti]" placeholder="Notiz #4" value="[{$ecs_notes4_ti}]"></a></li>
                <li[{if $ecs_notes_tab == "5"}] class="active"[{/if}] onclick="javascript:document.getElementById('activetab').value='5'"><a href="#notiz5" data-toggle="tab"><input type="text" class="editinput" size="10" name="editval[ecs_notes5_ti]" placeholder="Notiz #5" value="[{$ecs_notes5_ti}]"></a></li>
                <li[{if $ecs_notes_tab == "6"}] class="active"[{/if}] onclick="javascript:document.getElementById('activetab').value='6'"><a href="#notiz6" data-toggle="tab"><input type="text" class="editinput" size="10" name="editval[ecs_notes6_ti]" placeholder="Notiz #6" value="[{$ecs_notes6_ti}]"></a></li>
                <li[{if $ecs_notes_tab == "7"}] class="active"[{/if}] onclick="javascript:document.getElementById('activetab').value='7'"><a href="#notiz7" data-toggle="tab"><input type="text" class="editinput" size="10" name="editval[ecs_notes7_ti]" placeholder="Notiz #7" value="[{$ecs_notes7_ti}]"></a></li>
                <li[{if $ecs_notes_tab == "8"}] class="active"[{/if}] onclick="javascript:document.getElementById('activetab').value='8'"><a href="#notiz8" data-toggle="tab"><input type="text" class="editinput" size="10" name="editval[ecs_notes8_ti]" placeholder="Notiz #8" value="[{$ecs_notes8_ti}]"></a></li>
                [{if $ecs_notes_tiny}]
                    <li onclick="tinymce.each(tinymce.editors, function(editor) { if(editor.isHidden()) { $('.sourcehidden').show() } else { $('.sourcehidden').hide() } }); tinymce.each(tinymce.editors, function(editor) { if(editor.isHidden()) { editor.show(); } else { editor.hide(); } });"[{* <-- gesehen im Modul blaTinyMCE *}]><a href="#" title="Source code"><span class="glyphicon glyphicon-menu-left"></span><span class="glyphicon glyphicon-menu-right"></span></a></li>
                [{/if}]
            </ul>
            [{* ################################################################################################################################################################## *}]
            <div class="tab-content">
                <br>
                <div class="tab-pane[{if $ecs_notes_tab == "1"}] active[{/if}]" id="notiz1">
                    <textarea class="txtfield" style="min-width:100%;min-height:400px;font-size:15px;line-height:1.5em;" name="editval[ecs_notes]" [{ $readonly }]>[{$ecs_notes}]</textarea>
                </div>
                <div class="tab-pane[{if $ecs_notes_tab == "2"}] active[{/if}]" id="notiz2">
                    <textarea class="txtfield" style="min-width:100%;min-height:400px;font-size:15px;line-height:1.5em;" name="editval[ecs_notes2]" [{ $readonly }]>[{$ecs_notes2}]</textarea>
                </div>
                <div class="tab-pane[{if $ecs_notes_tab == "3"}] active[{/if}]" id="notiz3">
                    <textarea class="txtfield" style="min-width:100%;min-height:400px;font-size:15px;line-height:1.5em;" name="editval[ecs_notes3]" [{ $readonly }]>[{$ecs_notes3}]</textarea>
                </div>
                <div class="tab-pane[{if $ecs_notes_tab == "4"}] active[{/if}]" id="notiz4">
                    <textarea class="txtfield" style="min-width:100%;min-height:400px;font-size:15px;line-height:1.5em;" name="editval[ecs_notes4]" [{ $readonly }]>[{$ecs_notes4}]</textarea>
                </div>
                <div class="tab-pane[{if $ecs_notes_tab == "5"}] active[{/if}]" id="notiz5">
                    <textarea class="txtfield" style="min-width:100%;min-height:400px;font-size:15px;line-height:1.5em;" name="editval[ecs_notes5]" [{ $readonly }]>[{$ecs_notes5}]</textarea>
                </div>
                <div class="tab-pane[{if $ecs_notes_tab == "6"}] active[{/if}]" id="notiz6">
                    <textarea class="txtfield" style="min-width:100%;min-height:400px;font-size:15px;line-height:1.5em;" name="editval[ecs_notes6]" [{ $readonly }]>[{$ecs_notes6}]</textarea>
                </div>
                <div class="tab-pane[{if $ecs_notes_tab == "7"}] active[{/if}]" id="notiz7">
                    <textarea class="txtfield" style="min-width:100%;min-height:400px;font-size:15px;line-height:1.5em;" name="editval[ecs_notes7]" [{ $readonly }]>[{$ecs_notes7}]</textarea>
                </div>
                <div class="tab-pane[{if $ecs_notes_tab == "8"}] active[{/if}]" id="notiz8">
                    <textarea class="txtfield" style="min-width:100%;min-height:400px;font-size:15px;line-height:1.5em;" name="editval[ecs_notes8]" [{ $readonly }]>[{$ecs_notes8}]</textarea>
                </div>
            </div>
            [{* ################################################################################################################################################################## *}]
            <br>
            <input type="hidden" id="activetab" class="editinput" name="editval[ecs_notes_tab]" value="[{$ecs_notes_tab}]">
            <input type="submit" class="sourcehidden edittext [{* btn btn-danger btn-lg  *}]btn-block start" id="oLockButton" value="[{ oxmultilang ident="GENERAL_SAVE" }]" onclick="Javascript:document.myedit.fnc.value='save'" [{ $readonly }]>
            <br>
            <span style="float:right;">Proudly powered by <a href="https://ecomstyle.de" target="_blank"><strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;"><span style="color: #000000">ComStyle.de</span></strong></a></span>
            <div class="sourcehidden checkbox">
                <label>
                    <input type="checkbox" name="editval[ecs_notes_tiny]" value="1" [{if $ecs_notes_tiny}]checked="checked"[{/if}]> TinyMCE verwenden <small>(Einstellung wird nach dem Speichern angewandt)</small>
                </label>
            </div>
        </form>
    </div>
</body>