<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

namespace Ecs\Notizen\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Controller\Admin\AdminController;

class Notizen extends AdminController
{
    protected $_sThisTemplate = "ecs_notes.tpl";
    const CONFIG_MODULE_NAME  = 'module:ecs_notes';

    public function render()
    {
        $oConf                              = Registry::getConfig();
        $this->_aViewData['ecs_notes_tab']  = $oConf->getShopConfVar('ecs_notes_tab');
        $this->_aViewData['ecs_notes_tiny'] = $oConf->getShopConfVar('ecs_notes_tiny');
        $this->_aViewData['ecs_notes']      = $oConf->getShopConfVar('ecs_notes');
        $this->_aViewData['ecs_notes2']     = $oConf->getShopConfVar('ecs_notes2');
        $this->_aViewData['ecs_notes3']     = $oConf->getShopConfVar('ecs_notes3');
        $this->_aViewData['ecs_notes4']     = $oConf->getShopConfVar('ecs_notes4');
        $this->_aViewData['ecs_notes5']     = $oConf->getShopConfVar('ecs_notes5');
        $this->_aViewData['ecs_notes6']     = $oConf->getShopConfVar('ecs_notes6');
        $this->_aViewData['ecs_notes7']     = $oConf->getShopConfVar('ecs_notes7');
        $this->_aViewData['ecs_notes8']     = $oConf->getShopConfVar('ecs_notes8');
        $this->_aViewData['ecs_notes_ti']   = $oConf->getShopConfVar('ecs_notes_ti');
        $this->_aViewData['ecs_notes2_ti']  = $oConf->getShopConfVar('ecs_notes2_ti');
        $this->_aViewData['ecs_notes3_ti']  = $oConf->getShopConfVar('ecs_notes3_ti');
        $this->_aViewData['ecs_notes4_ti']  = $oConf->getShopConfVar('ecs_notes4_ti');
        $this->_aViewData['ecs_notes5_ti']  = $oConf->getShopConfVar('ecs_notes5_ti');
        $this->_aViewData['ecs_notes6_ti']  = $oConf->getShopConfVar('ecs_notes6_ti');
        $this->_aViewData['ecs_notes7_ti']  = $oConf->getShopConfVar('ecs_notes7_ti');
        $this->_aViewData['ecs_notes8_ti']  = $oConf->getShopConfVar('ecs_notes8_ti');
        return parent::render();
    }

    public function save()
    {
        $oConf   = Registry::getConfig();
        $aParams = Registry::getConfig()->getRequestParameter("editval");
        $sShopId = $oConf->getShopId();
        $oConf->saveShopConfVar('arr', 'ecs_notes_tab', $aParams['ecs_notes_tab'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes_tiny', $aParams['ecs_notes_tiny'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes', $aParams['ecs_notes'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes2', $aParams['ecs_notes2'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes3', $aParams['ecs_notes3'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes4', $aParams['ecs_notes4'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes5', $aParams['ecs_notes5'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes6', $aParams['ecs_notes6'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes7', $aParams['ecs_notes7'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes8', $aParams['ecs_notes8'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes9', $aParams['ecs_notes9'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes_ti', $aParams['ecs_notes_ti'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes2_ti', $aParams['ecs_notes2_ti'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes3_ti', $aParams['ecs_notes3_ti'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes4_ti', $aParams['ecs_notes4_ti'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes5_ti', $aParams['ecs_notes5_ti'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes6_ti', $aParams['ecs_notes6_ti'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes7_ti', $aParams['ecs_notes7_ti'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes8_ti', $aParams['ecs_notes8_ti'], $sShopId, self::CONFIG_MODULE_NAME);
        $oConf->saveShopConfVar('arr', 'ecs_notes9_ti', $aParams['ecs_notes9_ti'], $sShopId, self::CONFIG_MODULE_NAME);
    }

}
