Readme eComStyle.de Notizen
================================

Composer name
---------------
ecs/notizen


Geeignet für Shopversion
-------------------------
OXID eShop 6


Vorbereitung der Modulinstallation
-----------------------------------

Erstellen Sie via SSH-Client eine Verbindung mit dem Server, auf dem Ihr OXID eShop liegt.
Wechseln Sie in das OXID-Projektverzeichnis, in dem sich die Datei composer.json sowie die source- und vendor-Ordner befinden.
Führen Sie dort folgende Befehle aus (bitte jede Zeile einzeln ausführen):

    mkdir vendor/zip

    composer config repo.meinzip artifact ./vendor/zip


Bitte beachten:
Die o.g. Vorbereitung der Installation muss nur einmal ausgeführt werden.
Bei weiteren Modulinstallation kann direkt mit der Modulinstallation begonnen werden.


Modulinstallation
------------------

1. Loggen Sie sich via FTP auf dem Server ein und kopieren Sie die ZIP-Datei (das eComStyle.de-Modul) unverändert in den Ordner vendor/zip.

2. Erstellen Sie via SSH-Client eine Verbindung mit dem Server, auf dem Ihr OXID eShop liegt.
Wechseln Sie in das OXID-Projektverzeichnis, in dem sich die Datei composer.json sowie die source- und vendor-Ordner befinden.
Führen Sie dort folgenden Befehl aus:

    composer require ecs/notizen

Die dann folgenden Abfragen in der Console können einfach mit ENTER quittiert werden.

3. Loggen Sie sich anschließend in Ihren Shop-Admin ein und aktivieren das neue Modul unter Erweiterungen/Module.



Modulupdate
------------

1. Loggen Sie sich via FTP auf dem Server ein und kopieren Sie die ZIP-Datei (das eComStyle.de-Modul) unverändert in den Ordner vendor/zip.
Eine evtl. bereits vorhandene, gleichnamige ZIP-Datei (zB. eien alte Modulversion) kann einfach gelöscht werden.

2. Erstellen Sie via SSH-Client eine Verbindung mit dem Server, auf dem Ihr OXID eShop liegt.
Wechseln Sie in das OXID-Projektverzeichnis, in dem sich die Datei composer.json sowie die source- und vendor-Ordner befinden.
Führen Sie dort folgenden Befehl aus:

    composer update

Bei den dann folgenden Abfragen in der Console sollte sie für das upgedatete Modul die Frage "Do you want to overwrite them? (y/N)" mit "y" bestätigen.
Nur dann wird die alte Modulversion im Ordner source/modules/ecs durch die aktuelle Version ersetzt.

3. Loggen Sie sich anschließend in Ihren Shop-Admin ein, deaktivieren Sie das Modul einmal und aktivieren es dann wieder.
Dadurch werden ggf. neue Moduleinstellungen aktiviert, neue Datenbankfelder angelegt und der TMP geleert.




Lizenzinformationen
-------------------
Please retain this copyright header in all versions of the software

Copyright (C) Josef A. Puckl | eComStyle.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see {http://www.gnu.org/licenses/}.



Sonstige Wünsche
----------------
Sehr gerne sind wir Ihnen bei allen Aufgaben rund um Ihren Onlineshop behilflich!
Fragen Sie bitte jederzeit einfach an: <https://ecomstyle.de/kontakt/>